package Assignments.Thirteen;

import java.io.*;

public class PrintWriterReader {

    public static void main(String[] args) throws IOException {
        FileWriter fileWriter=new FileWriter("FileWriterReader.txt");
        PrintWriter pw=new PrintWriter(fileWriter);
        pw.write("Helloo /");
        pw.write("Namasakarr /");
        pw.write("Namastee /");
        pw.write("Kem choo");


        pw.close();


        FileReader fileReader=new FileReader("FileWriterReader.txt");
        int i;
        while((i=fileReader.read())!=-1){
            System.out.print((char)i);
        }



    }
}


/*
OUTPUT:
Helloo /Namasakarr /Namastee /Kem choo
 */