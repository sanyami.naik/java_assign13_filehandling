package Assignments.Thirteen;

public class EmployeeCommandLine {
    int id;
    int salary;
    String name;

    void acceptDetails(String name,int id,int salary)
    {

        this.name=name;
        this.id=id;
        this.salary=salary;

    }

    void displayDetails(){
        System.out.println("The name of the employee is "+name);
        System.out.println("The id of the employee is "+id);
        System.out.println("The salary of the employee is "+salary);
    }


    public static void main(String[] args) {
        EmployeeCommandLine employeeCommandLine=new EmployeeCommandLine();
        System.out.println("Enter the name,id and salary of the employee");

        String name=args[0];
        int id=Integer.parseInt(args[1]);
        int salary=Integer.parseInt(args[2]);
        employeeCommandLine.acceptDetails( name,id, salary);

        employeeCommandLine.displayDetails();


    }
}



/*OUTPUT:
Enter the name,id and salary of the employee
The name of the employee is Sanyami
The id of the employee is 65
The salary of the employee is 23456

 */