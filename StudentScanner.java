package Assignments.Thirteen;

import java.util.Scanner;

public class StudentScanner {

    public static void main(String[] args) {


        String name;
        int id;
        float marks;
        Scanner sc = new Scanner(System.in);

        System.out.println("Sequence- Name ,id ,marks");
        System.out.println("Enter the name of Student");
        System.out.println("Enter the id of Student");
        System.out.println("Enter the marks of Student");

        name = sc.nextLine();
        id = sc.nextInt();
        marks=sc.nextFloat();

        System.out.println("The name of student is " + name + " with id " + id + " has scored " + marks);


        System.out.println("************************************************");

        System.out.println("Sequence- id ,marks,name");
        System.out.println("Enter the id of Student");
        System.out.println("Enter the marks of Student");
        System.out.println("Enter the name of Student");

        id = sc.nextInt();
        marks=sc.nextFloat();
        sc.nextLine();
        name = sc.nextLine();

        System.out.println("The name of student is " + name + " with id " + id + " has scored " + marks);



    }
}



/*
OUTPUT:

Sequence- Name ,id ,marks
Enter the name of Student
Enter the id of Student
Enter the marks of Student
sanyami
45
90
The name of student is sanyami with id 45 has scored 90.0
************************************************
Sequence- id ,marks,name
Enter the id of Student
Enter the marks of Student
Enter the name of Student
34
67
sanyami
The name of student is sanyami with id 34 has scored 67.0

 */

