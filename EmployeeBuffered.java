package Assignments.Thirteen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class EmployeeBuffered {

    int id;
    int salary;
    String name;

    void acceptDetails() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the id");
        id = Integer.parseInt(br.readLine());


        System.out.println("Enter the name");
        name = br.readLine();


        System.out.println("Enter the salary");
        salary = Integer.parseInt(br.readLine());

    }

    void showDetails() {
        System.out.println("the Details are");
        System.out.println("The id is " + id + " with salary " + salary + "and name " + name);
    }

    public static void main(String[] args) {
        EmployeeScanner employee = new EmployeeScanner();
        employee.acceptDetails();
        employee.showDetails();
    }
}

/*OUTPUT:
Enter the id
34
Enter the name
Raj purohit
Enter the salary
34570
the Details are
The id is 34 with salary 34570and name Raj purohit

 */