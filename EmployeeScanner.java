package Assignments.Thirteen;
import practice.FileHandling.serialization.Employee;

import java.util.Scanner;
public class EmployeeScanner {
     int id;
     int salary;
     String name;

     void acceptDetails(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the id");
        id=scanner.nextInt();
        scanner.nextLine();


        System.out.println("Enter the name");
        name=scanner.nextLine();


        System.out.println("Enter the salary");
        salary=scanner.nextInt();

    }

    void showDetails(){
        System.out.println("the Details are");
        System.out.println("The id is "+id+" with salary "+salary+"and name "+name);
    }

    public static void main(String[] args) {
        EmployeeScanner employee=new EmployeeScanner();
        employee.acceptDetails();
        employee.showDetails();
    }
}





/*OUTPUT:
C:\Users\Coditas\.jdks\corretto-1.8.0_342\bin\java.exe "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\lib\idea_rt.jar=58239:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\charsets.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\access-bridge-64.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\cldrdata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\dnsns.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jaccess.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jfxrt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\localedata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\nashorn.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunec.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunjce_provider.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunmscapi.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunpkcs11.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\zipfs.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jce.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfr.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfxswt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jsse.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\management-agent.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\resources.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\rt.jar;C:\Users\Coditas\IdeaProjects\Project\out\production\Project Assignments.Thirteen.EmployeeScanner
Enter the id
34
Enter the name
Raj Purohit
Enter the salary
24356789
the Details are
The id is 34 with salary 24356789and name Raj Purohit

 */
















