package Assignments.Thirteen;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileClosing{
        public static void main(String[] args) throws IOException {
            FileWriter fileWriter = new FileWriter("FileClosing.txt");
            PrintWriter pw = new PrintWriter(fileWriter);
            pw.write("Helloo /");
            pw.write("Namasakarr /");
            pw.write("Namastee /");
            pw.write("Kem choo");


            pw.close();

        }
}




//closing of file is a good practice as if we dont close it the file will just be created and it wont write any context to it
